# UI Design
![logo](_media/undraw_design_tools_42tf1.svg 'size=05%')

UI/UX are the visual and functional dimensions of your products. We help you achieve a very balanced and usable interface, fast by rapid experimentation and prorotyping.

## UI
1. Requirement Gathering & Information Architecture 
2. Wireframe
3. Visual Hierarchies & Style Guide
4. Low Fidelity
5. High Fidelity (Interactive) Prototyping 
6. Design Testing & Delievery

## Packages
1. Concept/MVP
2. Functional UI - HTML/CSS 


[Write to us](mailto:hello.designb@gmail.com?subject=UI%20Work&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A)


------------------------------------

# Web & Mobile
![logo](_media/undraw_react_y7wq1.svg 'size=05%')

We use tools that puts your products in hands of your users fast, targeting maximum platforms in shorter amount of time. While you gather feedback and imagine changes we implement them for you.

## Product Develpment
1. Concept/MVP
2. Web Development
3. Mobile App
4. Analytics and App usage data 
5. Cloud deployment and Scalability 
6. Data Processing or Repoting at Big Data Scale

## Tech Skills
1. NodeJS
2. ReactJS
3. HTML/CSS
4. BigData
5. Python

[Write to us](mailto:hello.designb@gmail.com?subject=Devlopment%20Web/Mobile&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A)

------------------------------------
# Digital Media Studio
![logo](_media/undraw_photo_session_clqr1.svg 'size=05%')

People do not buy goods and services. They buy relationships, stories and magic - Seth Godin. Videos  are a very powerful instrument to narrate your brand’s story, to showcase your service, and to engage with your audience. 

## 2D & 3D animation
1. Scripting 
2. Storyboard / Character Design / Voiceover
3. Voiceover
4. Animation
5. Sound Design
6. Motion Graphics

## Photograpghy & Videography
1. Photo Editing
2. Pre-Production
3. Filming, Equipment & Acquisition
4. Post-Production, Editing, Graphics, Narration
5. Music Licensing

[Write to us](mailto:hello.designb@gmail.com?subject=Digital%20Media&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A)

------------------------------------
# Fine Arts
![logo](_media/undraw_fall_thyk1.svg 'size=05%')

‘Art’ is with us, from the beginning of time. From caves to modern day media something that didn’t change is art. It has always spoken the same thing. Art expresses you, your point of view and at times its can be more effective than your elevator pitch. 

1. Portraits
2. Mandalas
3. Sketching
4. Wall Painting
5. Sculpture
6. Custom Design 
7. Illustrations

[Write to us](mailto:hello.designb@gmail.com?subject=Fine%20Arts&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A)

------------------------------------
# Print Media
![logo](_media/undraw_master_plan_95wa1.svg 'size=05%')

Media is a way of getting attention. We help you to come up with media that sends a message across to your coustomers and investors, grabbing their attention and making them to perform the action you want. 

1. Flex
2. Hoarding
3. Signage
4. Promotion (digital and print)
5. Canvas Print 
6. Customized Prints 
7. Advertisement (Facebook, Insta etc..)

[Write to us](mailto:hello.designb@gmail.com?subject=Print%20Media&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A)


# Say hi
We are a space where we belive tech and design can solve problems, you can [Mail Us](mailto:hello.designb@gmail.com?subject=Hi%20Team&amp;body=Hey%20Designb%2C%0A%0ACan%20you%20please%20provide%20me%20an%20estimate%20for%20%3Cmy%20idea%3E%20%0A) or follow us at [Instagram](https://www.instagram.com/designb.studio/) to stay updated about tech and arts stuff happenning  around the world.
